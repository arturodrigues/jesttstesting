import mock from 'xhr-mock';
import createMockStore, { MockStoreCreator } from 'redux-mock-store';
/// <reference path="./redux/redux-thunk.d.ts" />
/// <reference path="./redux/redux.d.ts" />

type mock = MockStoreCreator<cnv.store.IInitialData, Redux.ThunkDispatch<any, undefined, Redux.Action>>;
describe('cnv main actions', () => {
  beforeEach(() => mock.setup());

  afterEach(() => mock.teardown());

  it('fetch initial data', () => {
    let workplaces: iw.models.IDetails[] = [{ id: cnv.WorkplaceType.Action, title: "Actions", imageId: "inbox", tagClasses: "actions" },
    { id: cnv.WorkplaceType.Case, title: "Cases", imageId: "folder-open", tagClasses: "cases" }];
    console.log(workplaces)
    let mockInitialDataStore: mock = createMockStore([ReduxThunk.default, Redux.simpleLogger])
    let urlRegExp: RegExp = new RegExp(".+\/rest\.oms\/cnv\/.+");
    expect.assertions(2);

    let initialData: cnv.models.IInitialData = {
      actionDefinitions: [{
        isCaseRelevant: false,
        caseId: "FetchInitialData",
        conceptId: "Integration Test"
      }]
    }

    mock.get(urlRegExp, (req, res) => {
      expect(req.header('Content-Type')).toEqual('application/json');
      return res.status(201).body(initialData);
    });

    const expectedActions = [
      { type: cnv.core.actions.ActionType.RequestInitialData },
      { type: cnv.core.actions.ActionType.SetWorkplaces, workplaces },
      { type: cnv.core.actions.ActionType.ReceiveInitialData, initialData }
    ]
    let state: cnv.store.IInitialData = {};
    const store = mockInitialDataStore(state);
    let endpoints: cnv.IEndpoints;
    return store.dispatch(cnv.core.actions.Main.fetchInitialData(endpoints.initialdata)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})