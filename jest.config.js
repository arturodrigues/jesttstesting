module.exports = {
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  preset: 'ts-jest',
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"]
};