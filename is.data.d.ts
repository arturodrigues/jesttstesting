declare namespace is.data {
	interface IFetchable<TData = object> {
		isFetching?: boolean;
		didInvalidate?: boolean;
		fetchDate?: number;
		lastUpdated?: number;
		data?: TData;
	}
}