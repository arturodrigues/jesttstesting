declare namespace is {
	interface IMap<T> {
		[key: string]: T;
	}
}