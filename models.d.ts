declare namespace cnv.models {
	interface IInitialData {
		actionsFilters?: iw.models.IDetails[];
		actionsSearchForm?: iw.models.IForm;
		actionDefinitions?: IActionDefinition[];
		actionImplementations?: iw.models.IDetails[];
		casesFilters?: iw.models.IDetails[];
		casesSearchForm?: iw.models.IForm;
		concepts?: iw.models.IDetails[];
		settings?: IServerSetting;
	}
	interface IServerSetting {
		googleMapsApiKey?: String;
	}
	interface IActionDefinition extends iw.models.IDetails {
		isCaseRelevant?: boolean;
		paths?: string[][];
		conceptId?: string;
		implementationId?: string;
		caseId?: string;
		goalId?: string;
	}
	interface IAction extends iw.models.IForm, iw.models.IDetails {
		rules?: iw.models.ITable;
		knowledgeBase?: object;
		notes?: object;
		caseId?: string;
	}
	interface IGoal extends iw.models.IDetails {
		state?: number;
		stateName?: string;
	}
	interface ICase extends iw.models.IDetails {
		paths?: string[][];
		goals?: IGoal[];
		actionsSearchForm?: iw.models.IForm;
	}
	interface IActionsParameters extends IListParameters {
		filter?: string;
		case?: string;
		goal?: string;
	}
    
    export interface IListParameters {
		search?: string,
		skip?: number,
		top?: number,
		orderby?: string
	}
}