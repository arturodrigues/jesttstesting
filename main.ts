namespace cnv.core.actions {

	export enum ActionType {
		RequestInitialData = "REQUEST_INITIAL_DATA",
		ReceiveInitialData = "RECEIVE_INITIAL_DATA",
		SetWorkplaces = "SET_WORKPLACES",
		SelectWorkplace = "SELECT_WORKPLACE"
	}

	export interface IMainAction extends Redux.Action<ActionType> {
		workplaces?: iw.models.IDetails[];
		initialData?: models.IInitialData;
		workplaceId?: string;
	}

	export class Main {
		private static requestInitialData(): IMainAction {
			return { type: ActionType.RequestInitialData };
		}
		private static receiveInitialData(initialData: models.IInitialData): IMainAction {
			return { type: ActionType.ReceiveInitialData, initialData };
		}
		private static setWorkplaces(workplaces: iw.models.IDetails[]): IMainAction {
			return { type: ActionType.SetWorkplaces, workplaces };
		}
		static fetchInitialData(endpoint: string): Redux.ThunkAction<Promise<IMainAction>, store.IRoot, void, IMainAction> {
			return (dispatch: Redux.Dispatch) => {
				dispatch(Main.requestInitialData());
				dispatch(Main.setWorkplaces([
					{ id: WorkplaceType.Action, title: "Actions", imageId: "inbox", tagClasses: "actions" },
					{ id: WorkplaceType.Case, title: "Cases", imageId: "folder-open", tagClasses: "cases" }
				]));
				return new Promise((resolve, reject) => {
                    let initialData: cnv.models.IInitialData = {
                      actionDefinitions: [{
                            isCaseRelevant: false,
                            caseId: "FetchInitialData",
                             conceptId: "Integration Test"
                      }]
                    }
					
					resolve(dispatch(Main.receiveInitialData(initialData)));
					
				});
			};
		}
		static selectWorkplace(workplace: iw.models.IDetails): IMainAction {
			return { type: ActionType.SelectWorkplace, workplaceId: workplace.id };
		}
	}

}