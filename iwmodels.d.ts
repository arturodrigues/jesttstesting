declare namespace iw.models {
	interface IDetails IId {
		title?: string;
		subTitle?: string;
		information?: INameValuePair[];
		dataSource?: any;
		tagClasses?: string;
	}
	interface ITable extends IForm {
		data?: object[];
		primaryKeys?: string[];
		total?: number;
	}
	interface IControl {
		type?: string;
	}
	interface IId {
		$id?: string;
		id?: string;
	}
	interface INameValuePair {
		name?: string;
		value?: any;
	}
	interface IFormField {
		details?: IDetails;
		control?: IControl;
		inputScope?: string;
		valueIcons?: string[];
		fieldId?: string;
		detailsMapping?: string|string[];
		placeholderText?: string;
	}
	interface IForm extends IId {
		formId?: string;
		fields?: IFormField[];
		itemTemplate?: IFormTemplate;
	}
	interface IFormTemplate {
		commands?: IAction[];
	}
}