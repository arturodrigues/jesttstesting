declare namespace cnv.store {
	export type IInitialData = is.data.IFetchable<cnv.models.IInitialData>;
	export type IWorkplaces = ITable<iw.models.IDetails[], string>;
	export interface ISearchableTable extends ITable<is.data.IFetchable<iw.models.ITable>, string> {
		selectedFilterId?: string;
		searchParameters?: is.IMap<any>;
	}
	export type IActions = ISearchableTable;
	export type ICases = ISearchableTable;
	export interface ICaseDetails {
		selectedViewId?: number;
		selectedCaseId?: string;
		selectedGoalId?: string;
		actions?: ITable<is.data.IFetchable<iw.models.ITable>, string>;
		action?: is.data.IFetchable<cnv.models.IAction>;
		case?: is.data.IFetchable<cnv.models.ICase>;
	}
	export interface IActionOverview {
		form?: is.data.IFetchable<cnv.models.IAction>;
		selectedViewId?: number;
	}
	export interface IAction {
		overview?: IActionOverview;
		caseDetails?: ICaseDetails;
	}
	export interface ICaseOverview {
		details?: is.data.IFetchable<cnv.models.ICase>;
		selectedViewId?: number;
	}
	export interface ICase {
		overview?: ICaseOverview;
		details?: ICaseDetails;
	}
	export interface IRoot {
		initialData?: IInitialData;
		workplaces?: IWorkplaces;
		actions?: IActions;
		action?: IAction;
		cases?: ICases;
		case?: ICase;
	}
}