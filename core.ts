namespace cnv {
	export enum WorkplaceType {
		Action = "actions",
		Case = "cases"
	}
	export interface IEndpoints {
		initialdata: string;
	}
}